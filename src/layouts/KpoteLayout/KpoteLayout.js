import React from 'react'
import { Grid, Row, Col } from 'react-bootstrap'
import Header from '../../components/Header'
import Sidebar from 'containers/Sidebar/SidebarContainer'
import classes from './KpoteLayout.scss'
import '../../styles/core.scss'

export const KpoteLayout = ({ children }) => (
  <div>
    <Header />
    <Grid fluid>
      <Row>
        <Col sm={3} md={2} className={classes.sidebar}>
          <Sidebar />
        </Col>
        <Col sm={9} smOffset={3} md={10} mdOffset={2} className={classes.mainContainer}>
          <Row>
            <Col sm={12}>
              {children}
            </Col>
          </Row>
        </Col>
      </Row>
    </Grid>
  </div>
)

KpoteLayout.propTypes = {
  children : React.PropTypes.element.isRequired
}

export default KpoteLayout
