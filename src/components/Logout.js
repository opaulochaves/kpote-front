import React from 'react'
import { connect } from 'react-redux'
import { logout } from 'routes/Auth/modules/actions'

class Logout extends React.Component {
  componentDidMount () {
    this.props.dispatch(logout())
  }
  render () {
    return (
      <p>You are now logged out</p>
    )
  }
}

export default connect()(Logout)
