import React from 'react'
import {
  Modal,
  Row,
  Col,
  Button,
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock
} from 'react-bootstrap'
import { Field, reduxForm } from 'redux-form'

// This smells bed. You should DRY! Look into FormNewProject you did the same thing
function FieldGroup({
  id,
  label,
  help,
  input,
  placeholder,
  type,
  meta: { touched, error },
  ...props
}) {
  let helpText = touched && error ? error : help || null
  return (
    <FormGroup controlId={id} validationState={touched && error ? 'error' : null}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl {...input} type={type} placeholder={placeholder} />
      <HelpBlock>{helpText}</HelpBlock>
    </FormGroup>
  )
}

const FormMilestone = (props) => {
  const {
    toggleForm,
    open,
    fields : { name, description },
    error, handleSubmit, pristine, reset, submitting
  } = props

  return (
    <Modal show={open} onHide={toggleForm}>
      <form onSubmit={handleSubmit}>
        <Modal.Header closeButton>
          <Modal.Title>New Milestone</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Field
            {...name}
            id='name'
            name='name'
            type='text'
            label='Name'
            component={FieldGroup}
            help='This is a help text'
            placeholder='Enter name'
          />
          <Field
            {...description}
            id='description'
            name='description'
            type='text'
            label='Description'
            component={FieldGroup}
            help='This is a help text'
            placeholder='Enter a description'
          />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={toggleForm}>Close</Button>
          <Button
            bsStyle='primary'
            type='submit'
            disabled={pristine || submitting}
          >Save changes</Button>
        </Modal.Footer>
      </form>
    </Modal>
  )
}

FormMilestone.propTypes = {
  open       : React.PropTypes.bool.isRequired,
  toggleForm : React.PropTypes.func.isRequired
}

const validate = values => {
  const errors = {}
  if (!values.name) {
    errors.name = 'Required'
  } else if (values.name.length > 25) {
    errors.name = 'Must be 25 characters or less'
  }
  if (!values.description) {
    errors.description = 'Required'
  }
  return errors
}

export default reduxForm({
  form   : 'formMilestone',
  fields : ['name', 'description'],
  validate
})(FormMilestone)
