import React, { PropTypes } from 'react'
import { Panel, Row, Col } from 'react-bootstrap'
import { Link } from 'react-router'
import classes from './style.scss'

const header = (m) => (
  <h3>
    <Link to={`/milestones/${m.id}`}>
      {m.name}
    </Link>
  </h3>
)

const MilestoneList = ({ milestones }) => {
  if (!milestones || milestones.length === 0) {
    return <h3>No milestones!</h3>
  }

  const renderMilestones = () => milestones.map(m => (
    <Col key={m.id} sm={12} md={6} lg={4}>
      <Panel header={header(m)} bsStyle='primary' className={classes.panel}>
        {m.description}
      </Panel>
    </Col>
  ))

  return (
    <Row>
      {renderMilestones()}
    </Row>
  )
}

MilestoneList.propType = {
  milestones : PropTypes.array
}

export default MilestoneList
