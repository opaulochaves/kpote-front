import React, { PropTypes } from 'react'
import {
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock
} from 'react-bootstrap'

const FieldGroup = ({
  id,
  input,
  label,
  placeholder,
  type,
  help,
  field
}) => {
  let helpText = help
  if (!field.isValid) {
    helpText = field.errors.map((msg, i) => <span key={i}>{msg}<br /></span>)
  }
  return (
    <FormGroup controlId={id} validationState={!field.isValid ? 'error' : null}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl {...input} type={type} placeholder={placeholder} />
      <HelpBlock>{helpText}</HelpBlock>
    </FormGroup>
  )
}

FieldGroup.propTypes = {
  id          : PropTypes.string,
  label       : PropTypes.string,
  placeholder : PropTypes.string,
  input       : PropTypes.object,
  type        : PropTypes.string,
  help        : PropTypes.string,
  field       : PropTypes.object
}

export default FieldGroup
