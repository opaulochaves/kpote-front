import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { LinkContainer } from 'react-router-bootstrap'
import {
  Navbar,
  Nav,
  NavItem,
  Grid,
  Jumbotron,
  Button
} from 'react-bootstrap'
import classes from './Landing.scss'

const Landing = ({ auth }) => {
  return (
    <div>
      <Navbar fluid>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to={'/'}>Kpote Manager</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <LinkContainer to={{ pathname: '/about' }}>
              <NavItem eventKey={1}>About</NavItem>
            </LinkContainer>
          </Nav>
          <Nav pullRight>
            <LinkContainer to={{ pathname: '/login' }}>
              <NavItem eventKey={2}>Login</NavItem>
            </LinkContainer>
            <LinkContainer to={{ pathname: '/signup' }}>
              <NavItem eventKey={2}>Sign Up</NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      <Grid fluid>
        <Jumbotron>
          <h1>Kpote Manager</h1>
          <LinkContainer to={{ pathname: '/signup' }}>
            <Button bsStyle='primary'>Sign Up</Button>
          </LinkContainer>
        </Jumbotron>
      </Grid>
    </div>
  )
}

function mapStateToProps (state) {
  return {
    auth : state.auth
  }
}

export default connect(mapStateToProps)(Landing)
