import React from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import { Navbar, Nav, NavItem } from 'react-bootstrap'
import { IndexLink } from 'react-router'
import classes from './Header.scss'

export const Header = () => (
  <Navbar fluid fixedTop>
    <Navbar.Header>
      <Navbar.Brand>
        <IndexLink to='/' activeClassName={classes.activeRoute}>
          Kpote Manager
        </IndexLink>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav>
        <LinkContainer to={{ pathname: '/counter' }}>
          <NavItem>Counter</NavItem>
        </LinkContainer>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
)

export default Header
