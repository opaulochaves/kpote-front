// Returns a random number between min (inclusive) and max (exclusive)
// very useful to simulate different response times
const getRandomArbitrary = (min, max) =>
  (Math.random() * (max - min)) + min

const delay = (min = 150, max = 350) =>
  new Promise(resolve => setTimeout(resolve, getRandomArbitrary(min, max)))

export default delay
