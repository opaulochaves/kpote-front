import { combineReducers } from 'redux'
import { routerReducer as router } from 'react-router-redux'
import projectReducer from 'routes/Project/modules/project'
import authReducer from 'routes/Auth/modules/auth'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    // Add sync reducers here
    router,
    auth    : authReducer,
    project : projectReducer,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
