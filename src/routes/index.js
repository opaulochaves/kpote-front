import CounterRoute from './Counter'
import ProjectRoute from './Project'
import MilestoneRoute from './Milestone'
import Logout from 'components/Logout'
import About from 'components/About'
import Login from 'containers/Login'
import { ID_TOKEN } from 'utils/apiUtils'

function redirectToLogin (nextState, replace) {
  if (!localStorage.getItem(ID_TOKEN)) {
    replace({
      pathname : '/login',
      state    : { nextPathname: nextState.location.pathname }
    })
  }
}

function redirectToDashboard (nextState, replace) {
  if (localStorage.getItem(ID_TOKEN)) {
    replace('/')
  }
}

export const createRoutes = (store) => ({
  childRoutes : [
    { path: '/logout', component: Logout },
    { path: '/about', component: About },

    {
      onEnter     : redirectToDashboard,
      childRoutes : [
        // Unauthenticated routes
        // Redirect to dashboard if user is already logged in
        { path: '/login', component: Login }
        // ...
      ]
    },

    {
      path : '/',
      getComponent (nextState, cb) {
        if (localStorage.getItem(ID_TOKEN)) {
          return require.ensure([], (require) => {
            cb(null, require('../layouts/KpoteLayout/KpoteLayout').default)
          }, 'dashboard')
        }
        return require.ensure([], (require) => {
          cb(null, require('components/Landing').default)
        }, 'landing')
      },
      indexRoute : {
        getComponent (nextState, cb) {
          if (localStorage.getItem(ID_TOKEN)) {
            return require.ensure([], (require) => {
              cb(null, require('./Home').default)
            })
          }
          return cb()
        }
      },
      childRoutes : [
        {
          onEnter     : redirectToLogin,
          childRoutes : [
            // Protected nested routes for the dashboard
            CounterRoute(store),
            ProjectRoute(store),
            MilestoneRoute(store)
          ]
        }
      ]
    }

  ]
})

export default createRoutes
