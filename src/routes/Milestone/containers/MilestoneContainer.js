import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import {
  fetchMilestoneIfNeeded,
  invalidateMilestone,
  selectMilestone,
  getSelectedMilestone
} from '../modules/actions'

class MilestoneContainer extends Component {
  static propTypes = {
    params     : PropTypes.object,
    dispatch   : PropTypes.func,
    milestone  : PropTypes.object,
    isFetching : PropTypes.bool
  }

  constructor (props) {
    super(props)
    this.handleRefreshClick = this.handleRefreshClick.bind(this)
  }

  componentDidMount () {
    const { dispatch, params: { id } } = this.props
    dispatch(fetchMilestoneIfNeeded(id))
  }

  componentWillReceiveProps (nextProps) {
    console.log(this.props)
    if (this.props.params.id !== nextProps.params.id) {
      const { dispatch, params: { id } } = nextProps
      dispatch(fetchMilestoneIfNeeded(id))
      dispatch(selectMilestone(id))
    }
  }

  handleRefreshClick (e) {
    e.preventDefault()

    const { dispatch, params: { id } } = this.props
    dispatch(invalidateMilestone(id))
    dispatch(fetchMilestoneIfNeeded(id))
  }

  render () {
    const { isFetching, milestone } = this.props
    console.log(milestone)
    return (
      <div>
        {isFetching && !milestone &&
          <h4>Loading...</h4>
        }
        {!isFetching && !milestone &&
          <h4>No milestone.</h4>
        }
        {!isFetching &&
          <a href='#' onClick={this.handleRefreshClick}>Refresh</a>
        }
        {milestone &&
          <div>
            <h2>{milestone.name}</h2>
            <p>{milestone.description}</p>
          </div>
        }
      </div>
    )
  }
}

function mapStateToProps (state) {
  const {
    milestone,
    isFetching,
    error
  } = getSelectedMilestone(state) || {
    milestone  : {},
    isFetching : true,
    error      : null
  }
  return { isFetching, milestone, error }
}

export default connect(mapStateToProps)(MilestoneContainer)
