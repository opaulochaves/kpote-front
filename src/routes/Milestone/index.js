import { injectReducer } from 'store/reducers'

export default (store) => ({
  path : '/milestones/:id',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Milestone = require('./containers/MilestoneContainer').default
      const reducer = require('./modules/milestone').default

      /*  Add the reducer to the store on key 'project'  */
      injectReducer(store, { key: 'milestones', reducer })

      /*  Return getComponent   */
      cb(null, Milestone)

    /* Webpack named bundle   */
    }, 'milestone')
  }
})
