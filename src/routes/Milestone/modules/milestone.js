import { combineReducers } from 'redux'

import {
  MILESTONE_REQUEST,
  MILESTONE_FAILURE,
  MILESTONE_SUCCESS,
  MILESTONE_INVALIDATE,
  SELECT_MILESTONE
} from './actions'

const initialState = {
  isFetching    : false,
  didInvalidate : false,
  milestone     : {},
  error         : null
}

const MILESTONE_ACTION_HANDLERS = {
  [MILESTONE_REQUEST] : (state = initialState) => ({
    ...state,
    isFetching    : true,
    didInvalidate : false
  }),
  [MILESTONE_INVALIDATE] : (state = initialState) => ({
    ...state,
    didInvalidate : true
  }),
  [MILESTONE_SUCCESS] : (state = initialState, action) => ({
    ...state,
    milestone     : action.milestone,
    isFetching    : false,
    didInvalidate : false
  }),
  [MILESTONE_FAILURE] : (state = initialState, action) => ({
    ...state,
    isFetching    : false,
    didInvalidate : false,
    error         : action.error
  })
}

function selectedMilestone (state = null, action) {
  switch (action.type) {
    case SELECT_MILESTONE:
      return action.id
    default:
      return state
  }
}

function byId (state = {}, action) {
  const handler = MILESTONE_ACTION_HANDLERS[action.type]
  if (handler) {
    return {
      ...state,
      [action.id] : handler(state[action.id], action)
    }
  }
  return state
}

const rootReducer = combineReducers({
  byId,
  selected : selectedMilestone
})

export default rootReducer
