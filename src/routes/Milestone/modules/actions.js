import { callApi } from 'utils/apiUtils'
const API_ROOT = 'http://localhost:5100'

export const MILESTONE_REQUEST = 'MILESTONE_REQUEST'
export const MILESTONE_FAILURE = 'MILESTONE_FAILURE'
export const MILESTONE_SUCCESS = 'MILESTONE_SUCCESS'
export const MILESTONE_INVALIDATE = 'MILESTONE_INVALIDATE'
export const SELECT_MILESTONE = 'SELECT_MILESTONE'

export function selectMilestone (id) {
  return { type: SELECT_MILESTONE, id }
}

export function invalidateMilestone (id) {
  return { type: MILESTONE_INVALIDATE, id }
}

function milestoneRequest (id) {
  return { type: MILESTONE_REQUEST, id }
}

function milestoneSuccess (id) {
  return milestone => ({
    type : MILESTONE_SUCCESS,
    id,
    milestone
  })
}

function milestoneFailure (id) {
  return error => ({
    type : MILESTONE_FAILURE,
    id,
    error
  })
}

function fetchMilestone (id, projectId) {
  // TODO if there's no id, dispatch failure action
  const url = `${API_ROOT}/projects/${projectId}/milestones/${id}`
  return callApi(url, null, milestoneRequest(id), milestoneSuccess(id), milestoneFailure(id))
}

function shouldFetchMilestone (state, id) {
  const milestone = state.byId[id] || undefined
  if (!milestone) {
    return true
  } else if (milestone.isFetching) {
    return false
  } else {
    return milestone.didInvalidate
  }
}

export function fetchMilestoneIfNeeded (id) {
  return (dispatch, getState) => {
    let projId = getState().projectsById.selectedProject
    if (shouldFetchMilestone(getState().milestones, id)) {
      dispatch(selectMilestone(id))
      return dispatch(fetchMilestone(id, projId))
    }
  }
}

export function getSelectedMilestone (state) {
  const { selected, byId } = state.milestones
  if (selected && byId[selected]) {
    return byId[selected]
  }
  return {}
}
