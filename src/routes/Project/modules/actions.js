import { callApi } from 'utils/apiUtils'

export const PROJECTS_REQUEST = 'PROJECTS_REQUEST'
export const PROJECTS_FAILURE = 'PROJECTS_FAILURE'
export const PROJECTS_SUCCESS = 'PROJECTS_SUCCESS'
export const PROJECT_ADD = 'PROJECT_ADD'
export const PROJECT_UPDATE = 'PROJECT_UPDATE'
export const PROJECT_SAVE_FAILURE = 'PROJECT_SAVE_FAILURE'
export const PROJECT_DETAIL_RECIEVE = 'PROJECT_DETAIL_RECIEVE'
export const PROJECT_TOGGLE_FORM = 'PROJECT_TOGGLE_FORM'
export const PROJECTS_INVALIDATE = 'PROJECTS_INVALIDATE'

export function invalidateProjects () {
  return { type: PROJECTS_INVALIDATE }
}

export function toggleForm () {
  return { type: PROJECT_TOGGLE_FORM }
}

function projectsRequest () {
  return { type: PROJECTS_REQUEST }
}

function projectsSuccess () {
  return projects => ({
    type : PROJECTS_SUCCESS,
    projects
  })
}

function projectAddSuccess () {
  return project => ({
    type : PROJECT_ADD,
    project
  })
}

function projectsFailure () {
  return error => ({
    type : PROJECTS_FAILURE,
    error
  })
}

function fetchProjects () {
  return callApi('/projects', {
    method : 'GET'
  }, projectsRequest(), projectsSuccess(), projectsFailure())
}

export function saveProject (project) {
  const config = {
    method : 'POST',
    body   : JSON.stringify(project)
  }
  return callApi('/projects', config, projectsRequest(), projectAddSuccess(), projectsFailure())
}

function shouldFetchProjects (state) {
  if (state.project.projects.length === 0) {
    return true
  } else if (state.project.isFetching) {
    return false
  } else {
    return state.project.didInvalidate
  }
}

export function fetchProjectsIfNeeded () {
  return (dispatch, getState) => {
    if (shouldFetchProjects(getState())) {
      return dispatch(fetchProjects())
    }
  }
}
