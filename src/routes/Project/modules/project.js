import {
  PROJECTS_REQUEST,
  PROJECTS_FAILURE,
  PROJECTS_SUCCESS,
  PROJECT_ADD,
  PROJECT_TOGGLE_FORM,
  PROJECTS_INVALIDATE
} from './actions'

const PROJECT_ACTION_HANDLERS = {
  [PROJECTS_REQUEST] : (state) => ({
    ...state,
    isFetching    : true,
    didInvalidate : false
  }),
  [PROJECTS_INVALIDATE] : (state) => ({
    ...state,
    didInvalidate : true
  }),
  [PROJECTS_SUCCESS] : (state, action) => ({
    ...state,
    projects      : action.projects,
    isFetching    : false,
    didInvalidate : false
  }),
  [PROJECT_ADD] : (state, action) => ({
    ...state,
    projects      : [...state.projects, action.project],
    isFetching    : false,
    didInvalidate : false,
    error         : null
  }),
  [PROJECTS_FAILURE] : (state, action) => ({
    ...state,
    isFetching    : false,
    didInvalidate : false,
    error         : action.error
  }),
  [PROJECT_TOGGLE_FORM] : (state) => ({
    ...state,
    formOpen : !state.formOpen
  })
}

const initialState = {
  isFetching    : false,
  didInvalidate : false,
  projects      : [],
  error         : null,
  formOpen      : false,
  current       : null
}

export default function (state = initialState, action) {
  const handler = PROJECT_ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export const getProjects = (state) => state.projects

// TODO check if it comes with details
export const getCurrentProject = (state) => {
  if (state.projects && state.current !== null) {
    return state.projects.find(p => p.id === state.current)
  }
  return null
}
