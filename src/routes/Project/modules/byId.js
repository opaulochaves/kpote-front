import { combineReducers } from 'redux'

import {
  PROJECT_REQUEST,
  PROJECT_FAILURE,
  PROJECT_SUCCESS,
  PROJECT_INVALIDATE,
  SELECT_PROJECT
} from './actionsById'

const initialState = {
  isFetching    : false,
  didInvalidate : false,
  project       : {},
  error         : null
}

const PROJECT_ACTION_HANDLERS = {
  [PROJECT_REQUEST] : (state = initialState) => ({
    ...state,
    isFetching    : true,
    didInvalidate : false
  }),
  [PROJECT_INVALIDATE] : (state = initialState) => ({
    ...state,
    didInvalidate : true
  }),
  [PROJECT_SUCCESS] : (state = initialState, action) => ({
    ...state,
    project       : action.project,
    isFetching    : false,
    didInvalidate : false
  }),
  [PROJECT_FAILURE] : (state = initialState, action) => ({
    ...state,
    isFetching    : false,
    didInvalidate : false,
    error         : action.error
  })
}


function selectedProject (state = null, action) {
  switch (action.type) {
    case SELECT_PROJECT:
      return action.id
    default:
      return state
  }
}

function byId (state = {}, action) {
  const handler = PROJECT_ACTION_HANDLERS[action.type]
  if (handler) {
    return {
      ...state,
      [action.id] : handler(state[action.id], action)
    }
  }
  return state
}

const rootReducer = combineReducers({
  byId,
  selectedProject
})

export default rootReducer
