import { callApi } from 'utils/apiUtils'

export const PROJECT_REQUEST = 'PROJECT_REQUEST'
export const PROJECT_FAILURE = 'PROJECT_FAILURE'
export const PROJECT_SUCCESS = 'PROJECT_SUCCESS'
export const PROJECT_INVALIDATE = 'PROJECT_INVALIDATE'
export const SELECT_PROJECT = 'SELECT_PROJECT'

export function selectProject (id) {
  return { type: SELECT_PROJECT, id }
}

export function invalidateProject (id) {
  return { type: PROJECT_INVALIDATE, id }
}

function projectRequest (id) {
  return { type: PROJECT_REQUEST, id }
}

function projectSuccess (id) {
  return data => ({
    type    : PROJECT_SUCCESS,
    id,
    project : data.projects[0] || null
  })
}

function projectFailure (id) {
  return error => ({
    type : PROJECT_FAILURE,
    id,
    error
  })
}

function fetchProject (id) {
  return callApi(`/projects/${id}`, null, projectRequest(id), projectSuccess(id), projectFailure(id))
}

function shouldFetchProject (state, id) {
  const project = state.byId[id] || undefined
  if (!project) {
    return true
  } else if (project.isFetching) {
    return false
  } else {
    return project.didInvalidate
  }
}

export function fetchProjectIfNeeded (id) {
  return (dispatch, getState) => {
    if (shouldFetchProject(getState().projectsById, id)) {
      return dispatch(fetchProject(id))
    }
  }
}

// SELECTOR
export function getSelectedProject (state) {
  const { selectedProject, byId } = state.projectsById
  if (selectedProject && byId[selectedProject]) {
    return byId[selectedProject]
  }
  return {}
}
