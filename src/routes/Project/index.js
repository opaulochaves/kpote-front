import { injectReducer } from 'store/reducers'

// TODO for now this is an async route. Later, consider making it a sync one

export default (store) => ({
  path : '/projects/:id',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Project = require('./containers/ProjectContainer').default
      const reducer = require('./modules/byId').default

      /*  Add the reducer to the store on key 'project'  */
      injectReducer(store, { key: 'projectsById', reducer })

      /*  Return getComponent   */
      cb(null, Project)

    /* Webpack named bundle   */
    }, 'project')
  }
})
