import React, { PropTypes } from 'react'
import reformed from 'react-reformed'
import compose from 'react-reformed/lib/compose'
import validateSchema from 'react-reformed/lib/validateSchema'
import {
  Modal,
  Button
} from 'react-bootstrap'
import FieldGroup from 'components/Input/FieldGroup'

const FormProject = ({
  toggleForm,
  open,
  bindInput,
  bindToChangeEvent,
  model,
  onSubmit,
  setProperty,
  schema }) => {
  const submitHandler = (e) => {
    e.preventDefault()
    onSubmit(model)
  }

  return (
    <Modal show={open} onHide={toggleForm}>
      <form onSubmit={submitHandler}>
        <Modal.Header closeButton>
          <Modal.Title>New Project</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <FieldGroup
            id='name'
            label='Name'
            help='This is a help text'
            placeholder='Enter name'
            type='text'
            input={{ ...bindInput('name') }}
            field={schema.fields.name}
          />
          <FieldGroup
            id='description'
            label='Description'
            help='This is a help text'
            placeholder='Enter description'
            type='text'
            input={{ ...bindInput('description') }}
            field={schema.fields.description}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={toggleForm}>Close</Button>
          <Button
            bsStyle='primary'
            type='submit'
            disabled={!schema.isValid}
          >Save changes</Button>
        </Modal.Footer>
      </form>
    </Modal>
  )
}

FormProject.propTypes = {
  open              : PropTypes.bool,
  toggleForm        : PropTypes.func,
  bindInput         : PropTypes.func,
  bindToChangeEvent : PropTypes.func,
  model             : PropTypes.object,
  setProperty       : PropTypes.func,
  schema            : PropTypes.object,
  onSubmit          : PropTypes.func
}

export default compose(
  reformed(),
  validateSchema({
    name : {
      type     : 'string',
      required : true
    },
    description : {
      type     : 'string',
      required : true
    }
  })
)(FormProject)
