import React, { Component, PropTypes } from 'react'
import { Tabs, Tab } from 'react-bootstrap'
import MilestoneList from 'components/Project/MilestoneList'
import classes from 'components/Project/style.scss'

class ProjectDetailTab extends Component {
  static propTypes = {
    project : PropTypes.object.isRequired
  }

  constructor (props) {
    super(props)

    this.handleSelect = this.handleSelect.bind(this)
  }

  handleSelect (key) {
    console.log(key)
  }

  render () {
    const { project } = this.props

    return (
      <Tabs
        defaultActiveKey={1}
        onSelect={this.handleSelect}
        id='project-detail-tab'
        className={classes.detailTab}>
        <Tab eventKey={1} title='Milestones'>
          <MilestoneList milestones={project.milestones} />
        </Tab>
        <Tab eventKey={2} title='Members'>
          List of Members
        </Tab>
      </Tabs>
    )
  }
}

export default ProjectDetailTab
