import React, { Component } from 'react'
import {
  Row,
  Col,
  Panel
} from 'react-bootstrap'

class Lists extends Component {

  render () {
    const { lists, milestoneId } = this.props

    const renderTasks = (list) => (
      list.tasks.map(t => (
        t.milestone_id === milestoneId &&
          <Panel>{t.title}</Panel>
      ))
    )

    return (
      <Row style={{ overflowX: 'auto' }}>
        {lists.map(list =>
          <Col sm={2}>
            <Panel header={list.name}>
              {renderTasks(list)}
            </Panel>
          </Col>
        )}
      </Row>
    )
  }
}

export default Lists
