import React, { Component, PropTypes } from 'react'
import {
  Tab,
  Nav,
  NavItem,
  NavDropdown,
  MenuItem,
  Row,
  Col,
  Glyphicon
} from 'react-bootstrap'
import Lists from './Lists'

class BoardTab extends Component {
  static propTypes = {
    project : PropTypes.object.isRequired
  }

  render () {
    const { project } = this.props

    const milestoneList = project.milestones.map(m =>
      <NavItem
        eventKey={`milestone-${m.id}`}
      >{m.name}</NavItem>
    )
    const milestonePane = project.milestones.map(m =>
      <Tab.Pane eventKey={`milestone-${m.id}`}>
        <h2>{m.name}</h2>
        <p>{m.description}</p>
        <Lists milestoneId={m.id} lists={project.lists} />
      </Tab.Pane>
    )

    return (
      <Tab.Container id='tab-board'>
        <Row className='clearfix'>
          <Col sm={12}>
            <Nav bsStyle='tabs'>
              <NavDropdown eventKey='milestone' title='Milestones'>
                {milestoneList}
                <MenuItem divider />
                <NavItem eventKey='milestone-add'>
                  <Glyphicon glyph='plus' />
                  Add Milestone
                </NavItem>
              </NavDropdown>
              <NavItem eventKey='members'>
                Members
              </NavItem>
            </Nav>
          </Col>
          <Col sm={12}>
            <Tab.Content animation>
              {milestonePane}
              <Tab.Pane eventKey='milestone-add'>
                Add a new milestone
              </Tab.Pane>
              <Tab.Pane eventKey='members'>
                List of Members
              </Tab.Pane>
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    )
  }
}

export default BoardTab
