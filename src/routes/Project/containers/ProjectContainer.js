import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import {
  fetchProjectIfNeeded,
  invalidateProject,
  getSelectedProject,
  selectProject
} from '../modules/actionsById'
import BoardTab from './BoartTab'

class ProjectContainer extends Component {
  constructor (props) {
    super(props)
    this.handleRefreshClick = this.handleRefreshClick.bind(this)
  }

  componentDidMount () {
    const { dispatch, params: { id } } = this.props
    dispatch(fetchProjectIfNeeded(id))
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.params.id !== nextProps.params.id) {
      const { dispatch, params: { id } } = nextProps
      dispatch(fetchProjectIfNeeded(id))
      dispatch(selectProject(id))
    }
  }

  handleRefreshClick (e) {
    e.preventDefault()

    const { dispatch, params: { id } } = this.props
    dispatch(invalidateProject(id))
    dispatch(fetchProjectIfNeeded(id))
  }

  render () {
    const { project, isFetching } = this.props

    return (
      <div>
        {isFetching && !project &&
          <h4>Loading...</h4>
        }
        {!isFetching && !project &&
          <h4>No project.</h4>
        }
        {!isFetching &&
          <a href='#' onClick={this.handleRefreshClick}>Refresh</a>
        }
        {project &&
          <div>
            <h2>{project.name}</h2>
            <p>{project.description}</p>
            <BoardTab project={project} />
          </div>
        }
      </div>
    )
  }
}

ProjectContainer.propTypes = {
  dispatch   : PropTypes.func.isRequired,
  project    : PropTypes.object.isRequired,
  isFetching : PropTypes.bool.isRequired,
  params     : PropTypes.object.isRequired
}

function mapStateToProps (state) {
  const {
    project,
    isFetching,
    error
  } = getSelectedProject(state) || {
    project    : {},
    isFetching : true,
    error      : null
  }
  return { isFetching, project, error }
}

export default connect(mapStateToProps)(ProjectContainer)
