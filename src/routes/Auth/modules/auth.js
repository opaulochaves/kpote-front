import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_SUCCESS
} from './actions'

import { loadUserProfile } from 'utils/apiUtils'

const initialState = {
  user       : null,
  password   : null,
  loggingIn  : false,
  loginError : null
}

function initializeState () {
  const userProfile = loadUserProfile()
  return Object.assign({}, initialState, { user: userProfile })
}

export default function auth (state = initializeState(), action = {}) {
  switch (action.type) {
    case LOGIN_REQUEST:
      return Object.assign({}, state, { loggingIn: true })
    case LOGIN_SUCCESS:
      return Object.assign({}, state, {
        loggingIn : false,
        user      : action.user
      })
    case LOGIN_FAILURE:
      return {
        ...state,
        loggingIn  : false,
        user       : null,
        loginError : action.error
      }
    case LOGOUT_SUCCESS:
      return {
        ...state,
        loggingOut : false,
        user       : null,
        loginError : null
      }
    default:
      return state
  }
}
