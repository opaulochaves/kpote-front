import {
  callApi,
  ID_TOKEN,
  setIdToken,
  removeIdToken,
  decodeUserProfile
} from 'utils/apiUtils'

export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILURE = 'LOGIN_FAILURE'

export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS'

function loginRequest (email) {
  return {
    type : LOGIN_REQUEST,
    email
  }
}

function loginSuccess (payload) {
  const idToken = payload[ID_TOKEN]
  setIdToken(idToken)
  const user = decodeUserProfile(idToken)
  return {
    type : LOGIN_SUCCESS,
    user : user
  }
}

function loginFailure (error) {
  removeIdToken()
  return {
    type : LOGIN_FAILURE,
    error
  }
}

export function login (email, password) {
  const config = {
    method : 'post',
    body   : JSON.stringify({ email, password })
  }

  return callApi('/auth/login', config, loginRequest(email), loginSuccess, loginFailure)
}

function logoutSuccess () {
  removeIdToken()
  return {
    type : LOGOUT_SUCCESS
  }
}

export function logout () {
  return dispatch => {
    dispatch(logoutSuccess())
  }
}
