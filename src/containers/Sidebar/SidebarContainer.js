import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Nav, NavItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import {
  fetchProjectsIfNeeded,
  invalidateProjects,
  toggleForm,
  saveProject
} from 'routes/Project/modules/actions'
import FormContainer from 'routes/Project/containers/Form'

class Sidebar extends Component {
  constructor (props) {
    super(props)
    this.handleRefreshClick = this.handleRefreshClick.bind(this)
    this.handleToggleForm = this.handleToggleForm.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount () {
    const { dispatch } = this.props
    dispatch(fetchProjectsIfNeeded())
  }

  handleRefreshClick (e) {
    e.preventDefault()

    const { dispatch } = this.props
    dispatch(invalidateProjects())
    dispatch(fetchProjectsIfNeeded())
  }

  handleToggleForm (e) {
    e.preventDefault()
    this.props.dispatch(toggleForm())
  }

  handleSubmit (model) {
    const { dispatch } = this.props
    dispatch(saveProject(model))
      .then(() => {
        dispatch(toggleForm())
      })
  }

  render () {
    const { projects, isFetching, formOpen } = this.props

    const renderList = projects.map(p =>
      <LinkContainer key={p.id} to={{ pathname: `/projects/${p.id}` }}>
        <NavItem eventKey={p.id}>{p.name}</NavItem>
      </LinkContainer>
    )

    return (
      <div>
        {isFetching && projects.length === 0 &&
          <h4>Loading...</h4>
        }
        {!isFetching && projects.length === 0 &&
          <h4>No projects.</h4>
        }
        {!isFetching &&
          <div>
            <a href='#' onClick={this.handleRefreshClick}>Refresh</a>
            {' - '}
            <a href='#' onClick={this.handleToggleForm}>New</a>
          </div>
        }
        {projects.length > 0 &&
          <div>
            <h2>Projects</h2>
            <Nav bsStyle='pills' stacked>
              {renderList}
            </Nav>
          </div>
        }
        <FormContainer
          onSubmit={this.handleSubmit}
          initialModel={{}}
          open={formOpen}
          toggleForm={this.handleToggleForm}
        />
      </div>
    )
  }
}

Sidebar.propTypes = {
  dispatch   : PropTypes.func.isRequired,
  projects   : PropTypes.array.isRequired,
  isFetching : PropTypes.bool.isRequired,
  formOpen   : PropTypes.bool.isRequired
}

function mapStateToProps (state) {
  const {
    projects,
    isFetching,
    formOpen
  } = state.project || {
    projects   : [],
    isFetching : true,
    formOpen   : false
  }

  return { isFetching, projects, formOpen }
}

export default connect(mapStateToProps)(Sidebar)
