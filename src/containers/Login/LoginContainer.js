import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import {
  Grid,
  Row,
  Col,
  Panel,
  FormGroup,
  InputGroup,
  Glyphicon,
  Button,
  Alert
} from 'react-bootstrap'
import { login } from 'routes/Auth/modules/actions'
import classes from './Login.scss'

class LoginContainer extends Component {
  static propTypes = {
    user       : PropTypes.object,
    loginError : PropTypes.object,
    dispatch   : PropTypes.func.isRequired,
    location   : PropTypes.object
  }

  static contextTypes = {
    router : PropTypes.object.isRequired,
    store  : PropTypes.object.isRequired
  }

  constructor (props) {
    super(props)
    this.handleLogin = this.handleLogin.bind(this)
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.user) {
      // logged in, let's show redirect if any, or show home
      try {
        const redirect = this.props.location.query.redirect
        this.context.router.replace(redirect)
      } catch (err) {
        this.context.router.replace('/')
      }
    }
  }

  handleLogin (event) {
    event.preventDefault()
    const email = this.refs.email
    const password = this.refs.password
    this.props.dispatch(login(email.value, password.value))
    email.value = ''
    password.value = ''
  }

  render () {
    const { user, loginError } = this.props
    return (
      <Grid className={classes.container}>
        <Row>
          <Col className={classes.title}>
            <h1>Kpote Manager</h1>
          </Col>
        </Row>
        <Row>
          <Col sm={12} md={6} lg={4} className={classes.loginForm}>
            <Panel header='Login'>
              <form onSubmit={this.handleLogin}>
                <FormGroup>
                  <InputGroup>
                    <InputGroup.Addon><Glyphicon glyph='user' /></InputGroup.Addon>
                    <input
                      type='email'
                      name='email'
                      ref='email'
                      className='form-control'
                      placeholder='Email (hint: kpote@test.com)'
                      required
                      autoFocus
                    />
                  </InputGroup>
                </FormGroup>

                <FormGroup>
                  <InputGroup>
                    <InputGroup.Addon><Glyphicon glyph='lock' /></InputGroup.Addon>
                    <input
                      type='password'
                      name='password'
                      ref='password'
                      className='form-control'
                      required
                    />
                  </InputGroup>
                </FormGroup>

                <Button type='submit' bsStyle='primary'>
                  Submit
                </Button>

                {!user && loginError &&
                  <Alert bsStyle='danger'>
                    <h4>Oh snap! You got an error!</h4>
                    <p>{loginError.message}</p>
                  </Alert>
                }
              </form>
            </Panel>
          </Col>
        </Row>
      </Grid>
    )
  }
}

function mapStateToProps (state) {
  const { auth } = state
  if (auth) {
    return {
      user       : auth.user,
      loginError : auth.loginError
    }
  }

  return { user: null }
}

export default connect(mapStateToProps)(LoginContainer)
